package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;
// long comparator because K will most likely be a long
public class KeyComp implements Comparator<Long>{

	@Override
	public int compare(Long o1, Long o2) {
		if (o1>o2) {
			return 1;
		}
		else if (o1<o2) {
			return -1;
		}
		return 0;
	}
	

}