package edu.uprm.cse.datastructures.cardealer;

import java.util.Comparator;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.KeyComp;
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.carlist.CarList;
import edu.uprm.cse.datastructures.cardealer.model.carlist.CarTable;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA.MapEntry;

@Path("/cars")
public class CarManager {

	private HashTableOA<Long, Car> myCarTable = CarTable.getInstance();

	// Add a car
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	//@Consumes(MediaType.APPLICATION_JSON)
	public Response addCar(Car c) {
		myCarTable.put(c.getCarId(), c);

		return Response.status(201).build();
	}

	// Get a car by ID.
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCarByID(@PathParam("id") long id) {
		if (myCarTable.get(id)!=null) {
			return myCarTable.get(id);
		}



		throw new WebApplicationException(404);
	}

	// Get all cars.
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] sol = new Car[myCarTable.getValues().size()];
		for (int i = 0; i < myCarTable.getValues().size(); i++) {
			sol[i] = myCarTable.getValues().get(i);
		}
		return sol;
	}

	// Update specific car.
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCarByID(@PathParam("id") long id, Car car) {
		if (myCarTable.contains(id)) {
			myCarTable.put(id, car);

			return Response.status(Response.Status.OK).build();
		}


		// if not found
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@DELETE
	@Path("{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCarByID(@PathParam("id") long id) {
		if (myCarTable.getKeys().contains(id)) {
			myCarTable.remove(id);
				return Response.status(Response.Status.OK).build();
		}	
		

		// if not found
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}