package edu.uprm.cse.datastructures.cardealer.model.carlist;
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

public class CarTable {
private static HashTableOA<Long, Car> carTable = new HashTableOA<>();
	
	private CarTable() {
		}
	
	
	public static HashTableOA<Long, Car> getInstance() {
		return carTable;
	}
	
	//empties the list of cars
	
	public static void resetCars() {
		carTable = new HashTableOA<>();
	}

}
