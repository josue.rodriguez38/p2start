package edu.uprm.cse.datastructures.cardealer.model.carlist;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	private static CircularSortedDoublyLinkedList<Car>carList = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	private CarList() {
		}
	
	
	public static CircularSortedDoublyLinkedList<Car> getInstance() {
		return carList;
	}
	
	//empties the list of cars
	
	public static void resetCars() {
		carList.clear();
	}
}
