package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.List;

import edu.uprm.cse.datastructures.cardealer.util.*;
import edu.uprm.cse.datastructures.cardealer.util.list.SortedList;
import edu.uprm.cse.datastructures.cardealer.model.*;

public class HashTableOA<K, V> implements Map<K, V> {

	// the MapEntry class that goes into the buckets
	public static class MapEntry<K,V> {
		private K key;
		private V value;
		// constructor
		public MapEntry(K key, V value) {
			super();
			this.key = key;
			this.setValue(value);
		}
		public K getKey() {
			return key;
		}
		public V getValue() {
			return value;
		}
		public void setValue(V value) {
			this.value = value;
		}
		public void setKey(K key) {
			this.key = key;			
		}


	}
	private Comparator<K> compK;
	private Comparator<V> compV;
	private int currentSize;
	private Object[] buckets;
	private static final int DEFAULT_BUCKETS = 50;

	// constructor with desired capacity as parameter
	public HashTableOA(int numBucket) {
		this.currentSize  = 0;
		this.compK = (Comparator<K>) new KeyComp();
		this.compV =  (Comparator<V>) new CarComparator();
		this.buckets = new Object[numBucket];
		for (int i =0; i < numBucket; ++i) {
			this.buckets[i] = new MapEntry<K,V>(null, null);
		}
	}
	//default constructor
	public HashTableOA() {
		this.compK = (Comparator<K>) new KeyComp();
		this.compV =  (Comparator<V>) new CarComparator();
		this.currentSize  = 0;
		this.buckets = new Object[DEFAULT_BUCKETS];
		for (int i =0; i < DEFAULT_BUCKETS; ++i) {
			this.buckets[i] = new MapEntry<K,V>(null, null);
		}
	}
	// FIRST hashfunction
	public int hashFunction(K key) {
		return ((3*(int) (Math.sqrt(key.hashCode()))) % this.buckets.length);
	}
	//SECOND function in case the first fails
	private int secondHashFunction(K key) {
		return ((7*(int) (Math.pow(key.hashCode(), 5))) % this.buckets.length);
	}
	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize==0;
	}

	// checks if element with key is in the table
	@Override
	public V get(K key) {
		int targetBucket = this.hashFunction(key);
		MapEntry<K,V> M = (MapEntry<K,V>)(this.buckets[targetBucket]);
		if (M.getKey()!=null) {
			if (M.getKey().equals(key)) {
				return (V) M.getValue();	

			}
			else  {
				targetBucket = this.secondHashFunction(key);	
				M = (MapEntry<K,V>)(this.buckets[targetBucket]);
				if (M.getKey()!=null) {
					if (M.getKey().equals(key)) {
						return  M.getValue();

					}	
				}

				else {
					for (int j = 0; j < this.buckets.length; j++) {
						M = (MapEntry<K,V>)(this.buckets[j]);
						if (M.getKey()!=null) {
							if (M.getKey().equals(key)) {
								return M.getValue();
							}
						}
					}
				}
				return null;
			}
		}
		return null;
	}

	// puts a new entry to the table - 
	//removes element with same key as the newEntry and returns it
	@Override
	public V put(K key, V value) {
		if (this.currentSize==(this.buckets.length-1)) {
			this.buckets=this.reAllocate();
		}
		MapEntry<K,V> newEntry = new MapEntry<K,V>(key, value);
		V result = null;
		MapEntry<K,V> first = (MapEntry<K,V>) this.buckets[hashFunction(key)];
		MapEntry<K,V> second = (MapEntry<K,V>) this.buckets[secondHashFunction(key)];

		if (first.getKey() == null ) {
			result = first.getValue();
			this.buckets[hashFunction(key)] = newEntry;
		}
		else if ( key.equals(first.getKey())) {
			result = first.getValue();
			this.buckets[hashFunction(key)] = newEntry;
			
		}

		else if (second.getKey() == null) {
			result = second.getValue();
			this.buckets[secondHashFunction(key)] = newEntry;;
		}
		else if (key.equals(second.getKey())) {
			result = second.getValue();
			this.buckets[secondHashFunction(key)] = newEntry;;	
		}
		else {
			for (int j = 0; j < this.buckets.length; j++) {
				MapEntry<K,V> M = (MapEntry<K,V>)(this.buckets[j]);
				if (M.getKey()==null) {
					this.buckets[j] = newEntry;
				}
			}
		}
		currentSize++;
		return result;
	}

	// Duplicates space of the bucket[] in the table and copys previous elements
	public Object[] reAllocate() {
		Object[] re = new Object[this.currentSize*3];
		boolean done = false;
		for (int i = 0; i < buckets.length; i++) {
			MapEntry<K,V> M = (MapEntry<K,V>)(this.buckets[i]);

			if (re[hashFunction(M.getKey())] != null) {
				re[hashFunction(M.getKey())] = this.buckets[i];
			}

			else if (re[secondHashFunction(M.getKey())] != null) {
				re[secondHashFunction(M.getKey())] = this.buckets[i];
			}
			else {
				for (int j = 0; j < re.length; j++) {
					if (re[j]!=null&& !done) {
						re[j] = this.buckets[i];
						done =true;
					}
				}
			}
			return re;	

		}
		return re;
	}
	// removes element with specific key
	@Override
	public V remove(K key) {
		int targetBucket = this.hashFunction(key);
		MapEntry<K,V> M = (MapEntry<K,V>)(this.buckets[targetBucket]);
		V res = null;
		if (key.equals(M.getKey())) {
			res = M.getValue();
			M.setValue(null);
			M.setKey(null);
			this.currentSize--;
			return res;
		}
		else  {
			targetBucket = this.secondHashFunction(key);	
			M = (MapEntry<K,V>)(this.buckets[targetBucket]);
			if (key.equals(M.getKey())) {

				res = M.getValue();
				M.setValue(null);
				M.setKey(null);					
				this.currentSize--;
				return res;
			}

			else {
				for (int j = 0; j < this.buckets.length; j++) {
					M = (MapEntry<K,V>)(this.buckets[j]);
					if (M.getKey()== key) {
						res = M.getValue();
						M.setValue(null);
						M.setKey(null);					
						this.currentSize--;
						return res;
					}
				}
			}

		}
		return res;
	}

// checks if mapEntry with given key exists in the table
@Override
public boolean contains(K key) {
	return this.get(key) != null;
}
// Returns a sorted lis of all keys in the hashtable
@Override
public SortedList<K> getKeys() {
	SortedList<K> result = new CircularSortedDoublyLinkedList<>(compK);
	for (int i = 0; i < buckets.length; i++) {
		MapEntry<K, V> M = (MapEntry<K,V>)(this.buckets[i]);
		if (M.getKey()!=null) {
			result.add(M.getKey());	
		}	
	} 


	return result;
}
// Returns a sorted lis of all values in the hashtable
@Override
public SortedList<V> getValues() {

	SortedList<V> result = new CircularSortedDoublyLinkedList<>(compV);
	for (int i = 0; i < buckets.length; i++) {
		MapEntry<K, V> M = (MapEntry<K,V>)(this.buckets[i]);
		if (M.getValue()!=null) {
			result.add(M.getValue());	
		}

	} 


	return result;
}

}
