
package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList.Node;
import edu.uprm.cse.datastructures.cardealer.util.list.SortedList;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

	//Creating the nodes for the list
		public static class Node<E> {
			private E element;
			private Node<E> next;
			private Node<E> prev;

			public Node(E e) {
				this.element = e;
				this.next = this.prev = null;

			}

			public E getElement() {
				return element;
			}

			public void setElement(E element) {
				this.element = element;
			}

			public Node<E> getNext() {
				return next;
			}

			public void setNext(Node<E> next) {
				this.next = next;
			}

			public Node<E> getPrev() {
				return prev;
			}

			public void setPrev(Node<E> prev) {
				this.prev = prev;
			}

		}
		// Iterator
		// Iterator
		private class CSDLLI<E> implements Iterator<E> {

			private Node<E> nextNode;

			public CSDLLI() {
				this.nextNode = (Node<E>) header.getNext();
			}

			@Override
			public boolean hasNext() {
				return !this.nextNode.equals(header);
			}

			@Override
			public E next() {
				if (this.hasNext()) {
					E res = (E) this.nextNode.getElement();
					this.nextNode = this.nextNode.getNext();
					return res;
				} else {
					throw new NoSuchElementException();
				}
			}

		}
	private Node<E> header;
	private int currentSize;
	private Comparator comp;

	public CircularSortedDoublyLinkedList(Comparator comp) {
		// TODO Auto-generated constructor stub
		this.currentSize = 0;
		this.comp = comp;
		this.header = new Node<>(null);
		this.header.setNext(this.header);
		this.header.setPrev(this.header);

	}
	@Override
	public Iterator iterator() {
		return new CSDLLI<E>();

	}
	// adds desired element onto list 
	@Override
	public boolean add(E e) {
		Node<E> addition = new Node(e);

		if (this.isEmpty()) {
			this.header.setNext(addition);
			this.header.setPrev(addition);
			addition.setNext(this.header);
			addition.setPrev(this.header);
		} else {

			// Checks if index is in bounds
			if (comp.compare(this.header.getPrev().getElement(), e)<= 0) {
				Node<E> test = this.header.getPrev();

				test.setNext(addition);

				this.header.setPrev(addition);

				addition.setPrev(test);
				addition.setNext(this.header);
			}
			else {
				Node<E> pin = this.header.getPrev();

				while (pin.getElement() != null) {

					if (comp.compare(pin.getElement(), e) <= 0)
						break;

					pin = pin.getPrev();
				}
				Node<E> test = pin.getNext();
				test.setPrev(addition);

				addition.setNext(test);
				addition.setPrev(pin);

				pin.setNext(addition);
			}

		}

		this.currentSize++;

		return true;
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	
	// calls the remove(index) with the firstIndex method as a parameter to remove specific element
	@Override
	public boolean remove(E e) {
		if (this.firstIndex(e)<0) {
			return false;
		}
		return this.remove(firstIndex(e));
		
	}
	

	// Removes the element at desired index
		public boolean remove(int index) {
			if (index < 0 || index >= this.currentSize)
				throw new IndexOutOfBoundsException("Index must be > 0 or < currentSize-1");

			Node<E> target;
			int count = 0;
			for (target = this.header.getNext(); count != index; target = target.getNext(), count++)
				;

			Node<E> prev = target.getPrev();
			Node<E> next = target.getNext();

			prev.setNext(next);
			next.setPrev(prev);
			this.currentSize--;

			target = null;

			return true;
		}

	@Override
	public int removeAll(E obj) {
		int count = 0;

		while (this.remove(obj)) {
			count++;
		}
		return count;
	}

	// returns element in first index
		@Override
		public E first() {
			return this.header.getNext().getElement();
		}

		// returns element in last index
		@Override
		public E last() {
			return this.header.getPrev().getElement();
		}


		// Return the element in index
		@Override
		public E get(int index) {
			if (index < 0 || index >= this.currentSize)
				throw new IndexOutOfBoundsException("Index does not exist in list");

			Node<E> target;
			int count = 0;
			for (target = this.header.getNext(); count != index; target = target.getNext(), count++)
				;
			return target.getElement();
		}


		// empties the list
		@Override
		public void clear() {
			Node<E> target = this.header.getNext();

			while (!target.equals(this.header)) {
				this.remove(target.getElement());

				target = target.getNext();
			}
		}


		// Checks if e is in the list
		@Override
		public boolean contains(E e) {
			Node<E> pin = this.header.getNext();
			while(pin.getElement()!=null) {
				if (pin.getElement().equals(e)) {
					return true;
				}
				pin = pin.getNext();
			}
			return false;
		}

		//Checks if list has an element
		@Override
		public boolean isEmpty() {
			return size() == 0;
		}
		// first index of element e
		@Override
		public int firstIndex(E e) {
			Node<E> pin = this.header.getNext();
			int count =0; 
			while(pin.getElement()!=null) {
				if (pin.getElement().equals(e)) {
					return count;
				}
				pin = pin.getNext();
				count++;
			}
			return -1;
		}


		// last index of element e
		@Override
		public int lastIndex(E e) {
			if (!(this.contains(e))) {
				return -1;
			}
			else {
				int res = this.size() - 1;
				Node<E> target = this.header.getPrev();

				while (target.getElement() != null) {

					if (target.getElement().equals(e))
						return res;

					target = target.getPrev();
					res--;
				}
			}
			return -1;
		}



}

